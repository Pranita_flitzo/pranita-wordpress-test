<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp5' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'oGgE$3u:^4DH}T&aHvt>Sd>M3Ox!eFj,u>2E(3YWNb3H&pw@-+%mPYoV( v^sRE?' );
define( 'SECURE_AUTH_KEY',  '{C:9o;_>Ey:*UdAu}1i525j1PRa$t,woR;us].-/uj[wdKCL9D/`5G)N4H~~;7c3' );
define( 'LOGGED_IN_KEY',    'S~@v>zx?o+Vr&Bw{]H^cLxrS5{4w $<ov^{.Jn6>c%T4b3uZ9VH-,SF[n7&paPnz' );
define( 'NONCE_KEY',        '-/9yL!F>bIkFhl-~b5pUhfZ%9B?)FMVHJ>k0f451{o H9?65JnkBHI7win#d#FX<' );
define( 'AUTH_SALT',        'Err,|Qvx>&2ay0Zwt4kN<x4XbRBg_g4<*eO]N+&vo52RxW:h3[GD#_t6s:yrEfis' );
define( 'SECURE_AUTH_SALT', '-Hh3ptvMrCndh$agaiC`=>;FZ<9In;IfyQ,1><eFmYydSVVot]{if{o:xD?[@)S?' );
define( 'LOGGED_IN_SALT',   'A%KN/qHGw-OYp$T`(Fk3h?EFLBc:;*Cg#ja%PH@EA(S]s~wSJViD_Ed9CTBu0isd' );
define( 'NONCE_SALT',       'B!s3^CHf^zs~v>J7`vd:Vh:)Of9p1z6xiK[{7MVNBAQR~.~ T<fvnQK$Gp,KjbAA' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

